import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Point;
import org.opencv.highgui.Highgui;

/**
 * Class Display sets the image that will be displayed in the panel
 * 
 */
public class Display extends JPanel {
	private static final long serialVersionUID = 1L;
	private static BufferedImage image;
	private static JFrame jframe = new JFrame("Object Detection from a file");

	// Constructor
	public Display() {
		jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Set a frame for the Display
		jframe.setSize(400, 400);
		jframe.setBackground(Color.BLACK);
		jframe.add(this, BorderLayout.CENTER);
		jframe.setVisible(true);
	}

	/*
	 * Draw the BufferedImage
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		BufferedImage img = this.image;

		if (img == null)
			return;
		g.drawImage(img, 5, 5, img.getWidth(), img.getHeight(), null);
	}

	/*
	 * Writes the BufferedImage into the image about to be display
	 */
	public static BufferedImage getDisplayImage() {
		return image;
	}

	/*
	 * Set the size of the frame
	 */
	public void setDisplaySize(int width, int height) {
		jframe.setSize(width, height);
	}

	/*
	 * Writes the BufferedImage into the image about to be display
	 */
	public static boolean setDisplayImage(Mat matrix) {
		image = (Frame.matToBufferedImage(matrix));
		return true;
	}

}// end of FacePanel class