import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

import javax.imageio.ImageIO;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

/**
 * Class Frame Manages and process the frames as matrices Summarizes all the
 * frames in a video
 */
public class Frame {
	// create a Image for a black background

	private ArrayList<Mat> consecu_frames = new ArrayList<Mat>();
	private HashMap<Integer, Integer> pix_vals = new HashMap<Integer, Integer>();

	private static int prev_index;

	private static String frames_path;

	private static final int PIXEL_THRESHOLD = 10;
	private static final int FRAME_RATIO = 10;

	public Frame() {

	}

	/*
	 * Sets a default Mat with black background
	 */
	public static void setMatZeros() {
		// mat = Mat.zeros(image.getHeight(), image.getWidth(), CvType.CV_8UC3);
		return;
	}

	/*
	 * Converts a Mat into a BufferedImage.
	 */
	public static BufferedImage matToBufferedImage(Mat matrix) {
		MatOfByte mb = new MatOfByte();
		Highgui.imencode(".jpg", matrix, mb);
		BufferedImage buff_img = null;

		try {
			buff_img = ImageIO.read(new ByteArrayInputStream(mb.toArray()));
		} catch (IOException e) {
			System.out.print("--(!)Error converting matrix to BufferedImage\n");
			e.printStackTrace();
			return buff_img; // Error
		}
		return buff_img; // Successful
	}

	/*
	 * Change a colored Mat image to grey
	 */
	public static void fromColorToGrey(Mat src, Mat dst) {
		Imgproc.cvtColor(src, dst, Imgproc.COLOR_BGR2GRAY);
		return;
	}

	/*
	 * Check if the frames captured are successors
	 */
	public void framesDetector(Mat input_frame) {
		Scalar color_channels;

		int sum_x = 0, sum_y = 0;
		double mean_x = 0, mean_y = 0;

		Mat prev_frame = new Mat();
		Mat curr_frame = new Mat();
		Mat frame_diff = new Mat();

		Mat img;

		ArrayList<Integer> summarized_frames = new ArrayList<Integer>();
		HashMap<Integer, Integer> sorted_pix_vals = new HashMap<Integer, Integer>();

		double img_per_frames;
		int pixel_count = 0;

		// Sets the 1st Frame
		if (consecu_frames.isEmpty()) {
			curr_frame = input_frame.clone();
			fromColorToGrey(curr_frame, curr_frame);
			consecu_frames.add(input_frame.clone());
			return;
		}

		// Store the current frame
		curr_frame = input_frame.clone();
		consecu_frames.add(input_frame.clone());

		// Get the previous frame through its index
		prev_index = consecu_frames.size() - 2;
		prev_frame = (consecu_frames.get(prev_index)).clone();

		// Convert image from Color to Grey
		fromColorToGrey(prev_frame, prev_frame);
		fromColorToGrey(curr_frame, curr_frame);

		// Subtract frames
		frame_diff = curr_frame.clone();
		Core.absdiff(prev_frame, curr_frame, frame_diff);

		// loops through image array,accesses pixels colors with .get()
		for (int i = 0; i < frame_diff.width(); i++) {
			for (int j = 0; j < frame_diff.height(); j++) {
				// Get the color of a pixel from the frame at those coordinates
				color_channels = new Scalar(frame_diff.get(j, i));

				// 10 is threshold; if color value is higher than 10,
				// pixel will be used for calculations.
				if (color_channels.val[0] > PIXEL_THRESHOLD) {
					pixel_count++;
					// sums up x and y coordinates of pixels identified
					sum_x += i;
					sum_y += j;
				}
			}
		}

		// Remove last frame that was duplicated
		if (pixel_count == 0) {
			consecu_frames.remove(consecu_frames.size() - 1);
			return;
		}

		mean_x = sum_x / pixel_count;
		mean_y = sum_y / pixel_count;

		// if calculations yield useful values then draw a circle at that spot
		float pix_thresh = (float) (frame_diff.width() * frame_diff.height() * 0.25);

		if (pixel_count < pix_thresh && mean_x < frame_diff.width() && mean_y < frame_diff.height()) {
			// System.out.println("Still in the same frame");
			Core.circle(input_frame, new Point(mean_x, mean_y), 6, new Scalar(0, 255, 0, 0), 2);
			pix_vals.put(consecu_frames.size(), pixel_count);

		} else if (consecu_frames.size() > 2) {
			// System.out.println("Frames ended\n\n");

			// 1 pic will summarize 10 pics
			img_per_frames = Math.ceil((double) (pix_vals.size()) / FRAME_RATIO);

			// Sort the pixel values of the frames in descending order
			// Iterate get the frames based on the index through them and stored
			// them
			sorted_pix_vals = sortByValues(pix_vals);
			Iterator it = sorted_pix_vals.entrySet().iterator();
			while (img_per_frames > 0 && it.hasNext()) {
				Map.Entry entry = (Map.Entry) it.next();
				Integer key = (Integer) entry.getKey();
				Integer val = (Integer) entry.getValue();

				// Add the "index" of the frame with the highest change in
				// pixels to a list
				summarized_frames.add(key);
				// Remaining amount of images
				img_per_frames--;
			}

			// Create folders and save first frame of the sequence found
			frames_path = FileHandler.createImageFolder("Summary");
			FileHandler.setFramePath(frames_path);
			img = consecu_frames.get(0);
			FileHandler.writeImg(frames_path, img);

			// Sort the frames that will be used to summarized
			Collections.sort(summarized_frames);
			Iterator<Integer> itr = summarized_frames.iterator();
			while (itr.hasNext()) {
				img = consecu_frames.get(itr.next());
				FileHandler.writeImg(frames_path, img);
			}

			// Reset everything
			consecu_frames.clear();
			pix_vals.clear();
			summarized_frames.clear();
		}

	}// end of checkFrameSequence

	/*
	 * Method that Sorts HashMap based on values
	 */
	private static HashMap<Integer, Integer> sortByValues(HashMap<Integer, Integer> map) {
		LinkedList list = new LinkedList(map.entrySet());
		// Defined Custom Comparator here
		Collections.sort(list, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				return ((Comparable) ((Map.Entry) (o2)).getValue()).compareTo(((Map.Entry) (o1)).getValue());
			}
		});

		// Here I am copying the sorted list in HashMap
		// using LinkedHashMap to preserve the insertion order
		HashMap sortedHashMap = new LinkedHashMap();
		for (Iterator it = list.iterator(); it.hasNext();) {
			Map.Entry entry = (Map.Entry) it.next();
			sortedHashMap.put(entry.getKey(), entry.getValue());
		}
		return sortedHashMap;
	}

}
