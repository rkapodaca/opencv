import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

/**
 * Abstract class Detector
 * 		Sets the classifier to which
 * 		Detects any given object
 * 
 */
public abstract class Detector {
	private CascadeClassifier cascade_classifier;

	/*
	 * Detector Constructor
	 */
	public Detector(String classifier) {
		cascade_classifier = new CascadeClassifier(classifier);
		// eyes_cascade = new CascadeClassifier(eyes_classifier_path);

		if (cascade_classifier.empty()) {
			System.out.println("--(!)Error loading classifier: " + classifier + "\n");
			return;
		} else {
			System.out.println("Loading Classifiers.....");
		}
	}

	/*
	 * Process Matrix through the classifiers
	 */
	public MatOfRect detectObjects(Mat input_frame) {
		Mat mRgba = new Mat();
		Mat mGrey = new Mat();
		MatOfRect objects = new MatOfRect();
		// NOTE: .read() (from main) returns image stored inside
		// the video capturing structure. It is not allowed to
		// modify or release the image! You can copy the frame and then do
		// whatever
		// you want with the copy.
		input_frame.copyTo(mRgba);

		// Convert image from Color to Grey
		Frame.fromColorToGrey(mRgba, mGrey);

		// Equalize the histogram of a gray scale image (mGrey)
		// through normalizing brightness and increases the contrast of image
		// for a better prediction
		Imgproc.equalizeHist(mGrey, mGrey);

		// Apply classifier to image
		cascade_classifier.detectMultiScale(mGrey, objects);

		return objects;
	}// end of detectAndDisplay()

	abstract Mat capture(Mat input_frame);
	abstract void mapObjts();
}// end of FaceDetector class