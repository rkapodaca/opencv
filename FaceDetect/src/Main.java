/*  
 * Captures the camera stream with OpenCV  
 * Search for the faces  
 * Display a circle around the faces using Java  
 */
import java.awt.*;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Vector;
import java.awt.image.DataBufferInt;

import javax.imageio.ImageIO;
import javax.swing.*;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.highgui.VideoCapture;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.objdetect.HOGDescriptor;

public class Main {

	public static void main(String arg[]) throws InterruptedException {

		final String root_videos = "./Videos/";

		// Scenario_ada/User2
		final String alejo_gp_t1 = root_videos + "alejo_gopro_web(Trim1).mp4";
		final String alejo_m_t1 = root_videos + "alejo_movil_web(Trim1).mp4";

		// Scenario_ada/User3
		final String ana_m_t1 = root_videos + "ana_movil_web(Trim1).mp4";
		final String ana_gp_t1 = root_videos + "ana_gopro_web(Trim1).mp4";
		final String ana_m_f = root_videos + "ana_movil_web_FULL.mp4";
		final String ana_gp_f = root_videos + "ana_gopro_web_FULL.mp4";

		// Scenario_i3a/User1
		final String alejan_m_t1 = root_videos + "alejandro_i3a_movil_web(Trim1).mp4";
		final String alejan_gp_t1 = root_videos + "alejandro_i3a_gopro_web(Trim1).mp4";
		final String alejan_m_t2 = root_videos + "alejandro_i3a_movil_web(Trim2).mp4";
		final String alejan_gp_t2 = root_videos + "alejandro_i3a_gopro_web(Trim2).mp4";

		// Other Videos
		final String video_harlem = root_videos + "Harlem Shake.mp4";
		final String video_harlem_faces = root_videos + "Harlem Shake(faces).mp4";
		final String video_walmart = root_videos + "Wal-mart_blackfriday.mp4";

		String curr_video = alejo_m_t1;

		int frame_count = 0;

		// Load the native library.
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

		// Create a display and a detector for images
		Display display = new Display();
		FaceDetector face_detector = new FaceDetector();

		// A matrix for reading image
		Mat webcam_image = new Mat();
		FileHandler metadata = new FileHandler(curr_video);

		Frame frame = new Frame();

		VideoCapture webCam = new VideoCapture(curr_video);

		if (webCam.isOpened()) {
			// Thread.sleep(200); /// This one-time delay allows the Webcam to
			// initialize itself

			while (true) {

				webCam.read(webcam_image);

				if (!webcam_image.empty()) {
					// set 1st frame
					// Thread.sleep(200); /// a 200 delay eases the
					// computational load .. with little performance leakage

					// Set frame for each captured image from video
					display.setDisplaySize(webcam_image.width() + 10, webcam_image.height() + 10);

					// Apply the classifier to the captured image
					webcam_image = face_detector.capture(webcam_image);
					frame.framesDetector(webcam_image);

					// Display the image to the panel
					display.setDisplayImage(webcam_image);
					Frame.setMatZeros();
					display.repaint();
					frame_count++;
				} else {
					System.out.println("Total faces detected: " + metadata.getTotalFaces());
					System.out.println("Total number of frames: " + frame_count);
					System.out.println("Frames reduced to: " + metadata.getTotalFrames());

					face_detector.mapObjts();
					display.repaint();
					System.out.println(" --(!) No captured frame from webcam !");
					break;
				}
			}
		}

		webCam.release(); // release the webcam

	} // end main

} // end of FacePanel