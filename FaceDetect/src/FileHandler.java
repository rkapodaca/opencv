import java.io.File;

import javax.swing.filechooser.FileSystemView;
import java.io.File;

import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;

public class FileHandler {
	private static String root_dir_imgs = "./Images Captured/";

	private static String faces_path = root_dir_imgs;
	private static String frames_path = root_dir_imgs;

	private static int num_of_frames;
	private static int num_of_faces;

	private static final String FRAME_NAME = "/Frame #";
	private static final String FACE_NAME = "/Face #";
	private static final String FACEMAP_NAME = "/facemap";
	private static final String FORMAT = ".jpg";

	/*
	 * Constructor takes the name of the file and creates a folder
	 */
	public FileHandler(String vid) {
		root_dir_imgs = root_dir_imgs + vid.substring(vid.lastIndexOf("/") + 1, vid.lastIndexOf(".")) + "/";
		File path = new File(root_dir_imgs);
		path.mkdirs();
	}
	
	/*
	 * Creates a folder for an image  
	*/
	public static String createImageFolder(String folder_name) {
		File path = new File(root_dir_imgs + folder_name);
		path.mkdirs();
		return path.toString();
	}

	public static void writeImg(String path, Mat img) {
		File folder = new File(path);
		String filename;

		if (folder.exists() == false) {
			folder.mkdirs();
		}

		if (path.equals(getFramePath())) {
			num_of_frames++;
			filename = FRAME_NAME + num_of_frames;
		} else if (path.equals(getFacePath())) {
			num_of_faces++;
			filename = FACE_NAME + num_of_faces;
		} else {
			filename = FACEMAP_NAME;
		}

		Highgui.imwrite(folder + filename + FORMAT, img);
	}

	public static int getTotalFaces() {
		return num_of_faces;
	}

	public static int getTotalFrames() {
		return num_of_frames;
	}

	public static String getRootDir() {
		return root_dir_imgs;
	}

	public static String getFacePath() {
		return faces_path;
	}

	public static String getFramePath() {
		return frames_path;
	}

	public static void setRootDir(String root_dir) {
		root_dir_imgs = root_dir;
	}

	public static void setFacePath(String face_p) {
		faces_path = face_p;
	}

	public static void setFramePath(String frame_p) {
		frames_path = frame_p;
	}
}
