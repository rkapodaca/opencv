import java.util.ArrayList;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

public class FaceDetector extends Detector {
	private static final String face_classifier_path = "./res/haarcascade_frontalface_alt_tree.xml";
	private static String faces_path;

	private ArrayList<Point> face_coor_tl = new ArrayList<Point>();
	private ArrayList<Point> face_coor_br = new ArrayList<Point>();

	private final Scalar FACE_COLOR = new Scalar(0, 255, 0, 255);

	public FaceDetector() {
		super(face_classifier_path);
	}

	/*
	 * 
	 */
	public Mat capture(Mat input_frame) {
		// Mat mRgba = input_frame;
		MatOfRect faces = new MatOfRect();

		faces = super.detectObjects(input_frame);

		if (faces.toArray().length != 0) {
			faces_path = FileHandler.createImageFolder("Faces Detected");
			FileHandler.setFacePath(faces_path);
		}

		// Iterate through all the objects detected by the classifier
		for (Rect rect_face : faces.toArray()) {
			/*
			 * To apply Eyes classifier to image
			 * face_eyes_cascade.detectMultiScale(faceROI, eyes, 1.1, 2, 0,new
			 * Size(30, 30), new Size()); num_of_eyes = eyes.toArray().length;
			 * ..
			 */

			// draw a green rectangle around face detected
			Core.rectangle(input_frame, rect_face.tl(), rect_face.br(), FACE_COLOR, 1);

			setFaceCoordinates(rect_face.tl(), rect_face.br());

			// Take the Grey image and "crop" the section where the face is
			// detected
			// Mat faceROI = mGrey.submat(rect_face);

			// Store faces detected on a home folder
			FileHandler.writeImg(faces_path, input_frame);

		} // end of Faces for loop

		return input_frame;
	}// end of detectAndDisplay()

	/*
	 * setter for list of coordinates from the faces captured
	 */
	private void setFaceCoordinates(Point top_left, Point bottom_right) {
		face_coor_tl.add(top_left);
		face_coor_br.add(bottom_right);
	}

	/*
	 * getter for list of coordinates from the faces captured
	 */
	public ArrayList<Point> getFaceCoordTL() {
		return face_coor_tl;
	}

	/*
	 * getter for list of coordinates from the faces captured
	 */
	public ArrayList<Point> getFaceCoordBR() {
		return face_coor_br;
	}

	/*
	 * Create a Map representing the total faces detected in the file for face
	 * detected a green cross is placed
	 */
	public void mapObjts() {
		ArrayList<Point> tl_coor = getFaceCoordTL();
		ArrayList<Point> br_coor = getFaceCoordBR();
		Point tl;
		Point br;
		int i = 0;
		int width, height;
		double[] pix_val;

		height = Display.getDisplayImage().getHeight();
		width = Display.getDisplayImage().getWidth();

		// create a Image with black background
		Mat grey_mat = Mat.zeros(height, width, CvType.CV_8UC1);

		while (tl_coor.size() > i) {
			// set the corner Points
			tl = tl_coor.get(i);
			br = br_coor.get(i);

			// Loop the rectangle starting at the top left
			// Get each pixel at coordinates and increment by 10
			for (int j = (int) tl.y; j < br.y; j++) {
				for (int k = (int) tl.x; k < br.x; k++) {

					pix_val = grey_mat.get(j, k);
					pix_val[0] = pix_val[0] + 10;
					grey_mat.put(j, k, pix_val);
				}
			}
			i++;
		}

		Display.setDisplayImage(grey_mat);
		FileHandler.writeImg(FileHandler.getRootDir(), grey_mat);

	}// end of faceMap()

}
